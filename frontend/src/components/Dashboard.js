import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import Popup from 'reactjs-popup';
import './../Dashboard.css'

class Dashboard extends Component {
    
    addCode() {
        alert('You will insert a code');
    }
    onChange(e) {
        this.setState({
            title: e.target.value
        });
    }
    updateText(text) {
        this.setState({
            title: `${this.state.title} ${text}`
        });
    }
    render() {
        
        return (<div>
            <div>
                <h1>Start a New Game</h1>
            </div>
            <div className="formContainer">
                <div className="publicRoom formHalf" >
                    <button class="buttonDash">
                        <Link to="/">Public Room</Link>
                    </button>
                </div>
                <div class="private_room formHalf">
                        <input type="text" class="privateCode" placeholder="Enter Code" />
                    <button class="buttonDash">
                        <Link to="/">Private Room</Link>
                    </button>
                </div>
            </div>
        </div>
        )
    }
}

export default Dashboard