import React, {Component} from 'react'
import Login from './Login'
import {HashRouter as Router, Route, Switch, Link} from 'react-router-dom';
import Register from './Register'

class Homepage extends Component {

  render() {
    return (
        <div>
                <div>
                    <h1>The Ultimate Alba-Neagra Experience</h1>
                    <h2>It`s Your Chance Now</h2>
                </div>
                 <div>
                  <ul className="buttonGroup">
                    <li>
                      <Link to="/dashboard">Play as Guest</Link>
                    </li>
                    <li>
                      <Link to="/login">Login</Link>
                    </li>
                    <li>
                      <Link to="/register">Register</Link>
                    </li>
                  </ul>
                  </div>

        </div>
    )
  }
}

export default Homepage