import React from 'react';
import {HashRouter as Router, Route, Switch, Link} from 'react-router-dom';
import './App.css';
import background from "./img/background.jpg";
import './homepage.css'
import Homepage from './components/Homepage'
import Login from './components/Login'
import Register from './components/Register'
import Dashboard from './components/Dashboard'
function App() {
    return (
        <div>
            <div className="container" style={{ backgroundImage: `url(${background})` }}>
                <Router>
                    <div>
                      <Route exact path="/" component={Homepage} />
                      <Route path="/login" component={Login} />
                      <Route path="/register" component={Register} />
                      <Route path="/dashboard" component={Dashboard} />
                    </div>
                </Router>
            </div>
        </div>
     );
    }

export default App;