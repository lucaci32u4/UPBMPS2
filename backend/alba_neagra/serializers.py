from abc import ABC

from rest_framework import serializers
from .models import User, Room


class UserRegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


class UserLoginSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'password']


class CreateRoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields = ['name', 'admin', 'noSlots']


class JoinRoomSerializer(serializers.Serializer):
    roomId = serializers.IntegerField()
    username = serializers.CharField(max_length=50)
