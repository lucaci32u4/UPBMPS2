from django.db import models


# Create your models here.

class User(models.Model):
    username = models.CharField(max_length=50)
    password = models.CharField(max_length=256)
    isGuest = models.BooleanField()


class Room(models.Model):
    roomId = models.IntegerField()
    name = models.CharField(max_length=50)
    state = models.CharField(max_length=10)
    admin = models.ForeignKey(User, on_delete=models.CASCADE, related_name='room_admin')
    player = models.ManyToManyField(User, related_name='room_players', blank=True)
    spectator = models.ManyToManyField(User, related_name='room_spectators', blank=True)
    noSlots = models.IntegerField()


class ScoreEntry(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='score_user')
    room = models.ForeignKey(Room, on_delete=models.CASCADE, related_name='score_room')
    score = models.IntegerField()
