from django.urls import path
from .views import UserRegisterViews
from .views import UserLoginViews
from .views import GuestLoginViews
from .views import SelfViews
from .views import CreateRoomViews
from .views import GetRoomViews
from .views import GetAllRoomsViews
from .views import JoinRoomViews

urlpatterns = [
    path('register/', UserRegisterViews.as_view(), name='register'),
    path('login/', UserLoginViews.as_view(), name='login'),
    path('login_guest/', GuestLoginViews.as_view(), name='login_guest'),
    path('self/', SelfViews.as_view(), name='self'),
    path('room/create', CreateRoomViews.as_view(), name='create_room'),
    path('room/<int:room_id>', GetRoomViews.as_view(), name='get_room'),
    path('rooms/', GetAllRoomsViews.as_view(), name='get_all_rooms'),
    path('rooms/join', JoinRoomViews.as_view(), name='join_room'),
]