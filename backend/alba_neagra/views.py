from django.shortcuts import render
from django.db.models import Max
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import UserRegisterSerializer
from .serializers import UserLoginSerializer
from .serializers import CreateRoomSerializer
from .serializers import JoinRoomSerializer
from .models import User, Room, ScoreEntry
from uuid import uuid1
from cryptography.fernet import Fernet

key = b'5Q38cUnx6C1pgJbULPfqdskIDil_1C9beVwgPeQtVXA='


# Create your views here.
class UserRegisterViews(APIView):
    def post(self, request):
        serializer = UserRegisterSerializer(data=request.data)
        if serializer.is_valid():
            response = Response({"status": "success", "data": serializer.validated_data}, status=status.HTTP_200_OK)
            if User.objects.filter(username=serializer.validated_data['username']).exists():
                response = Response({"status": "error", "data": "Existing username"})
            else:
                serializer.save()
            return response
        else:
            return Response({"status": "error", "data": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)


class UserLoginViews(APIView):
    def post(self, request):
        serializer = UserLoginSerializer(data=request.data)
        if serializer.is_valid():
            response = Response({"status": "error", "data": "Invalid username or password"})

            if User.objects.filter(username=serializer.validated_data['username']).exists():
                user = User.objects.get(username=serializer.validated_data['username'])
                if user.password == serializer.validated_data['password']:
                    response = Response({"status": "success", "data": "Login OK"}, status=status.HTTP_200_OK)

                    fernet = Fernet(key)
                    encrypted_token = fernet.encrypt(str(user.username).encode()).decode()
                    response.set_cookie('Auth-Token', encrypted_token)

            return response
        else:
            return Response({"status": "error", "data": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)


class GuestLoginViews(APIView):
    def post(self, request):
        guest = User.objects.create(username=uuid1(), password='reserved', isGuest=True)

        response = Response({"status": "success", "data": "Login OK"}, status=status.HTTP_200_OK)

        fernet = Fernet(key)
        encrypted_token = fernet.encrypt(str(guest.username).encode()).decode()
        response.set_cookie('Auth-Token', encrypted_token)
        return response


class SelfViews(APIView):
    def get(self, request):
        fernet = Fernet(key)
        username = fernet.decrypt(request.COOKIES.get('Auth-Token').encode()).decode()

        response = Response({"status": "success", "data": {"username": username}}, status=status.HTTP_200_OK)

        return response


class CreateRoomViews(APIView):
    def post(self, request):
        updated_data = request.data
        updated_data['admin'] = User.objects.get(username=request.data['admin']).id
        print(updated_data)
        serializer = CreateRoomSerializer(data=updated_data)
        if serializer.is_valid():
            if Room.objects.filter(name=serializer.validated_data['name']).exists():
                response = Response({"status": "error", "data": "Existing name"})
            else:
                last_id = Room.objects.aggregate(Max('roomId')).get('roomId__max')
                if last_id is None:
                    last_id = 0
                print(last_id)
                Room.objects.create(roomId=last_id + 1,
                                    name=serializer.validated_data['name'],
                                    state='waiting',
                                    admin=serializer.validated_data['admin'],
                                    noSlots=serializer.validated_data['noSlots'])
                response = Response({"status": "success", "data": {"roomId": last_id + 1}}, status=status.HTTP_200_OK)
            return response
        else:
            return Response({"status": "error", "data": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)


class GetRoomViews(APIView):
    def get(self, request, *args, **kwargs):
        room_id = self.kwargs['room_id']

        if Room.objects.filter(roomId=room_id).exists():
            current_room = Room.objects.get(roomId=room_id)

            players_list = {}
            for player in current_room.player.all():
                score = 0
                if ScoreEntry.objects.filter(user=player.id, room=current_room.id).exists():
                    score = ScoreEntry.objects.get(user=player.id, room=current_room.id).score
                players_list[player.username] = score

            spectators_list = {}
            for spectator in current_room.spectator.all():
                spectators_list[spectator.username] = 0

            response_data = {'roomId': room_id,
                             'name': current_room.name,
                             'state': current_room.state,
                             'admin': current_room.admin.username,
                             'player': players_list,
                             'spectator': spectators_list,
                             'noSlots': current_room.noSlots}

            response = Response({"status": "success", "data": response_data}, status=status.HTTP_200_OK)
        else:
            response = Response({"status": "error", "data": "Room Id not found"})
        return response


class GetAllRoomsViews(APIView):
    def get(self, request):
        complete_data = {}
        if Room.objects.filter(state='active').exists():

            active_data = {}

            active_rooms = Room.objects.filter(state='active').all()
            for active_room in active_rooms:

                # Get the list of players - get the usernames and find the scores
                players_list = {}
                for player in active_room.player.all():
                    score = 0
                    if ScoreEntry.objects.filter(user=player.id, room=active_room.id).exists():
                        score = ScoreEntry.objects.get(user=player.id, room=active_room.id).score
                    players_list[player.username] = score

                spectators_list = {}
                for spectator in active_room.spectator.all():
                    spectators_list[spectator.username] = 0

                active_data[active_room.roomId] = {'roomId': active_room.roomId,
                                                   'name': active_room.name,
                                                   'state': active_room.state,
                                                   'admin': active_room.admin.username,
                                                   'player': players_list,
                                                   'spectator': spectators_list,
                                                   'noSlots': active_room.noSlots}

            complete_data['active'] = active_data

        if Room.objects.filter(state='waiting').exists():

            waiting_data = {}

            waiting_rooms = Room.objects.filter(state='waiting').all()
            for waiting_room in waiting_rooms:

                # Get the list of players - get the usernames and find the scores
                players_list = {}
                for player in waiting_room.player.all():
                    score = 0
                    if ScoreEntry.objects.filter(user=player.id, room=waiting_room.id).exists():
                        score = ScoreEntry.objects.get(user=player.id, room=waiting_room.id).score
                    players_list[player.username] = score

                spectators_list = {}
                for spectator in waiting_room.spectator.all():
                    spectators_list[spectator.username] = 0

                waiting_data[waiting_room.roomId] = {'roomId': waiting_room.roomId,
                                                     'name': waiting_room.name,
                                                     'state': waiting_room.state,
                                                     'admin': waiting_room.admin.username,
                                                     'player': players_list,
                                                     'spectator': spectators_list,
                                                     'noSlots': waiting_room.noSlots}

            complete_data['waiting'] = waiting_data

        return Response({"status": "success", "data": complete_data}, status=status.HTTP_200_OK)


class JoinRoomViews(APIView):
    def post(self, request):
        serializer = JoinRoomSerializer(data=request.data)

        complete_data = {}

        if serializer.is_valid():

            if not User.objects.filter(username=serializer.validated_data['username']).exists():
                return Response({"status": "error", "data": "Username not valid"}, status=status.HTTP_200_OK)
            if not Room.objects.filter(roomId=serializer.validated_data['roomId']).exists():
                return Response({"status": "error", "data": "Room does not exist"}, status=status.HTTP_200_OK)

            current_user = User.objects.get(username=serializer.validated_data['username'])
            current_room = Room.objects.get(roomId=serializer.validated_data['roomId'])

            if current_room.noSlots == len(current_room.player.all()):
                current_room.spectator.add(current_user)
                complete_data['user_type'] = 'spectator'
            else:
                current_room.player.add(current_user)
                complete_data['user_type'] = 'player'

            return Response({"status": "success", "data": complete_data}, status=status.HTTP_200_OK)
        else:
            return Response({"status": "error", "data": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)
